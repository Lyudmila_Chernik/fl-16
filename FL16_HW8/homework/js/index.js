let isOperator = false;
let endOperator = /[/*+-]$/;
let isResult = false;

$('.calc-btn').on('click', function () {

    if (isResult) {
        $('.display').val('0').css('color', 'black');
        isResult = false;
    }

    if ($(this).attr('data-role') === 'operator') {
        if (isOperator) {
            let displayVal = $('.display').val();
            let replaced = displayVal.replace(endOperator, $(this).val());
            $('.display').val(replaced);
        } else {
            $('.display').val($('.display').val() + $(this).val());
            isOperator = true;
        }
    } else {
        if ($('.display').val() === '0') {
            $('.display').val('');
        }
        $('.display').val($('.display').val() + $(this).val());
        isOperator = false;
    }
});

$('.clear-btn').on('click', function () {
    $('.display').val('0');
    isOperator = false;
    isResult = false;
})

$('.calculate-btn').on('click', function () {
    let expression = $('.display').val();
    let result = calculate(expression);
    if (result === Infinity) {
        $('.display').val('ERROR').css('color', 'red');
    } else {
        $('.display').val(result);
        addLog(expression, result);
    }
    isResult = true;
})

function addLog(exp, res) {
    const expression = document.createElement('div');
    let fullExp = `${exp} = ${res}`;

    if (fullExp.includes('48')) {
        $(expression).css('text-decoration', 'underline');
    }

    $(expression).text(fullExp);
    const circleBtn = document.createElement('button');
    $(circleBtn).addClass('circle-btn');
    const delBtn = document.createElement('button');
    $(delBtn).addClass('del-btn').html('&#10006');
    const log = document.createElement('div');
    $(log).addClass('log');

    $(log).append(circleBtn, expression, delBtn);

    $('aside').prepend(log);

    $(delBtn).on('click', function () {
        $(delBtn).closest('div').remove();
    })

    $(circleBtn).on('click', function () {
        $(circleBtn).toggleClass('circle-btn-toggle');
    })
}

$('aside').scroll(function () {
    console.log(`Scroll Top: ${$(this).scrollTop()}`);
})

function evaluate(expression) {
    let numberArray = [];
    let operations = [];
    let number = '';
    for (let i = 0; i < expression.length; i++) {
        let character = +expression[i]
        if (!Number.isNaN(character)) {
            number = number + character;
        } else {
            numberArray.push(number);
            operations.push(expression[i]);
            number = '';
        }
        if (i === expression.length - 1) {
            numberArray.push(number);
        }
    }

    return numberArray.reduce((accumulator, currentValue, index) => {
        return calculate(accumulator, currentValue, operations[index - 1]);
    })
}

function calculate(firstValueString, secondValueString, operation) {
    let firstValue = Number.parseInt(firstValueString);
    let secondValue = Number.parseInt(secondValueString);
    let result;
    switch (operation) {
        case '+':
            result = firstValue + secondValue;
            break;
        case '-':
            result = firstValue - secondValue;
            break;
        case '*':
            result = firstValue * secondValue;
            break;
        case '/':
            result = firstValue / secondValue;
            break;
        default:
            result = firstValue + secondValue;
            break;
    }
    return result;
}