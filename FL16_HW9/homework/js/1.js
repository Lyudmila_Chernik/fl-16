function playGame({countUser, countComp, res, counters}) {
    counters.countU = 0;
    counters.countC = 0;
    res.innerText = 'Сделайте выбор';
    countUser.innerText = counters.countU;
    countComp.innerText = counters.countC;
    let fields = document.querySelectorAll('.field');
    fields.forEach(item => item.classList.remove('active', 'error'));
}

export default playGame;