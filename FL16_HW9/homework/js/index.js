import playGame from './1';
import {winner} from './2';


document.addEventListener('DOMContentLoaded', function () {
    const countUser = document.querySelector('.count-user'),
        countComp = document.querySelector('.count-comp'),
        userField = document.querySelector('.user-field'),
        compField = document.querySelector('.comp-field'),
        play = document.querySelector('.play'),
        out = document.querySelector('.out');

    let res = document.querySelector('.result');
    const counters = {
        countU: 0,
        countC: 0,
    };

    userField.addEventListener('click', choiceUser);

    play.addEventListener('click', function () {
        playGame({countUser, countComp, res, counters});
    });

    function choiceComp(userCh) {
        let rand = Math.floor(Math.random() * 3);
        let fields = compField.querySelectorAll('.field');
        compField.classList.add('blink');
        setTimeout(() => {
            compField.classList.remove('blink');
            let compCh = fields[rand].dataset.field;
            fields.forEach(item => item.classList.remove('active'));
            fields[rand].classList.add('active');
            winner({userCh, compCh, res, counters, countUser, countComp, compField, userField});
        }, 3000);
    }

    function choiceUser(e) {
        const target = e.target.closest('.field');
        if (target.classList.contains('field')) {
            let fields = document.querySelectorAll('.field');
            fields.forEach(item => item.classList.remove('active', 'error'));
            let userCh = target.dataset.field;
            target.classList.add('active');
            choiceComp(userCh);
        }
    }
});
