function winner({userCh, compCh, res, countU, counters, countUser, countComp, compField, userField}) {
    const comb = userCh + compCh;

    switch (comb) {
        case 'pp':
        case 'ss':
        case 'rr':
            res.innerText = 'Ничья!';
            break;

        case 'rs':
        case 'sp':
        case 'pr':
            res.innerText = 'Победили вы!';
            counters.countU++;
            countUser.innerText = counters.countU;
            compField.querySelector('[data-field="' + compCh + '"]').classList.add('error');
            break;

        case 'sr':
        case 'ps':
        case 'rp':
            res.innerText = 'Победил компьютер!';
            counters.countC++;
            countComp.innerText = counters.countC;
            userField.querySelector('[data-field="' + userCh + '"]').classList.add('error');
            break;

        default:
            break;
    }
}

export {winner};