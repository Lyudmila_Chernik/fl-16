'use strict';

class Pizza {

    constructor(size, type) {
        this.extraIngredients = [];
        let sizeIndex = this.allowedSizes.indexOf(size);
        if (arguments.length < 2) {

            throw new PizzaException('Required two arguments, given: ' + arguments.length);

        } else if (sizeIndex !== -1) {
            this.size = size;
        } else {
            throw new PizzaException('Invalid size');
        }

        let typeIndex = this.allowedTypes.indexOf(type);
        if (typeIndex !== -1) {
            this.type = type;
        } else {
            throw new PizzaException('Invalid type');
        }

    }

    addIngredients(ingredient) {

        let allowedExtraIngredientsIndex = this.allowedExtraIngredients.indexOf(ingredient);
        let extraIngredientsIndex = this.extraIngredients.indexOf(ingredient);

        if (arguments.length !== 1) {
            throw new PizzaException('Required one argument, given: ' + arguments.length);
        } else if (allowedExtraIngredientsIndex === -1) {
            throw new PizzaException('Invalid ingredient');
        } else if (extraIngredientsIndex !== -1) {
            throw new PizzaException('Duplicate ingredient');
        } else {
            this.extraIngredients.push(ingredient);
        }
    }

    removeIngredient(ingredient) {
        let allowedExtraIngredientsIndex = this.allowedExtraIngredients.indexOf(ingredient);
        let extraIngredientsIndex = this.extraIngredients.indexOf(ingredient);

        if (arguments.length !== 1) {
            throw new PizzaException('Required one argument, given: ' + arguments.length);
        } else if (allowedExtraIngredientsIndex === -1) {
            throw new PizzaException('Invalid ingredient');
        } else if (extraIngredientsIndex !== -1) {
            throw new PizzaException('Duplicate ingredient');
        } else {
            this.extraIngredients.splice(extraIngredientsIndex, 1);
        }
    }

    getSize() {
        return this.size;
    }

    getPrice() {
        let extraIngredientsPrice;
        if (this.extraIngredients.length === 0) {
            extraIngredientsPrice = 0
        } else {
            extraIngredientsPrice = this.extraIngredients.reduce(function (previousValue, currentValue) {
                return previousValue + currentValue.price;
            }, 0);
        }
        return this.size.price + this.type.price + extraIngredientsPrice;
    }

    getExtraIngredients() {
        return this.extraIngredients.map((element) => element.extra);
    }

    getPizzaInfo() {
        return `Size: ${this.size.size}, type: ${this.type.type}; 
        extra ingredients: ${this.getExtraIngredients()}; price: ${this.getPrice()}UAH.`
    }
}


class PizzaException {
    constructor(message) {
        this.log = message;
    }
}

Pizza.SIZE_S = { size: 'SMALL', price: 50 };
Pizza.SIZE_M = { size: 'MEDIUM', price: 75 };
Pizza.SIZE_L = { size: 'LARGE', price: 100 };

Pizza.TYPE_VEGGIE = { type: 'VEGGIE', price: 50 };
Pizza.TYPE_MARGHERITA = { type: 'MARGHERITA', price: 60 };
Pizza.TYPE_PEPPERONI = { type: 'PEPPERONI', price: 70 };

Pizza.EXTRA_TOMATOES = { extra: 'TOMATOES', price: 7 };
Pizza.EXTRA_CHEESE = { extra: 'CHEESE', price: 5 };
Pizza.EXTRA_MEAT = { extra: 'MEAT', price: 9 };

Pizza.allowedSizes = [Pizza.SIZE_S, Pizza.SIZE_M, Pizza.SIZE_L];
Pizza.allowedTypes = [Pizza.TYPE_VEGGIE, Pizza.TYPE_MARGHERITA, Pizza.TYPE_PEPPERONI];
Pizza.allowedExtraIngredients = [Pizza.EXTRA_TOMATOES, Pizza.EXTRA_CHEESE, Pizza.EXTRA_MEAT];


/**
 * Class
 * @constructor
 * @param size - size of pizza
 * @param type - type of pizza
 * @throws {PizzaException} - in case of improper use
 */
// function Pizza(size, type) { ... }

/* Sizes, types and extra ingredients */
// Pizza.SIZE_S = ...
// Pizza.SIZE_M = ...
// Pizza.SIZE_L = ...
// console.log(Pizza.SIZE_L);
// Pizza.TYPE_VEGGIE = ...
// Pizza.TYPE_MARGHERITA = ...
// Pizza.TYPE_PEPPERONI = ...

// Pizza.EXTRA_TOMATOES = ...
// Pizza.EXTRA_CHEESE = ...
// Pizza.EXTRA_MEAT = ...

// /* Allowed properties */
// Pizza.allowedSizes = ...
// Pizza.allowedTypes = ...
// Pizza.allowedExtraIngredients = ...


/**
 * Provides information about an error while working with a pizza.
 * details are stored in the log property.
 * @constructor
 */
// function PizzaException(...) { ... }


/* It should work */
// // small pizza, type: veggie
// let pizza = new Pizza(Pizza.SIZE_S, Pizza.TYPE_VEGGIE);
// // add extra meat
// pizza.addExtraIngredient(Pizza.EXTRA_MEAT);
// // check price
// console.log(`Price: ${pizza.getPrice()} UAH`); //=> Price: 109 UAH
// // add extra corn
// pizza.addExtraIngredient(Pizza.EXTRA_CHEESE);
// // add extra corn
// pizza.addExtraIngredient(Pizza.EXTRA_TOMATOES);
// // check price
// console.log(`Price with extra ingredients: ${pizza.getPrice()} UAH`); // Price: 121 UAH
// // check pizza size
// console.log(`Is pizza large: ${pizza.getSize() === Pizza.SIZE_L}`); //=> Is pizza large: false
// // remove extra ingredient
// pizza.removeExtraIngredient(Pizza.EXTRA_CHEESE);
// console.log(`Extra ingredients: ${pizza.getExtraIngredients().length}`); //=> Extra ingredients: 2
// console.log(pizza.getPizzaInfo()); //=> Size: SMALL, type: VEGGIE; extra ingredients: MEAT,TOMATOES; price: 114UAH.

// examples of errors
// let pizza = new Pizza(Pizza.SIZE_S); // => Required two arguments, given: 1

// let pizza = new Pizza(Pizza.SIZE_S, Pizza.SIZE_S); // => Invalid type

// let pizza = new Pizza(Pizza.SIZE_S, Pizza.TYPE_VEGGIE);
// pizza.addExtraIngredient(Pizza.EXTRA_MEAT);
// pizza.addExtraIngredient(Pizza.EXTRA_MEAT); // => Duplicate ingredient

// let pizza = new Pizza(Pizza.SIZE_S, Pizza.TYPE_VEGGIE);
// pizza.addExtraIngredient(Pizza.EXTRA_MEAT); // => Invalid ingredient





