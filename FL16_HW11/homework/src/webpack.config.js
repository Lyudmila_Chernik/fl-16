const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ImageminPlugin = require('imagemin-webpack');

const isDev = process.env.NODE_ENV === 'development';

const jsLoaders = () => {
  const loaders = [
    {
      loader: 'babel-loader',
    },
  ];

  if (isDev) {
    loaders.push('eslint-loader');
  }

  return loaders;
};

module.exports = {
  mode: 'development',
  entry: { main: ['./src/js/index.js', './src/scss/styles.scss'] },
  output: {
    filename: 'build.js',
    path: path.resolve(__dirname, 'dist'),
  },
  devServer: {
    port: 8080,
  },
  module: {
    rules: [
      {
        test: /.(s[ac]ss|css)$/,
        exclude: /node_modules/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
          },
          'css-loader',
          'sass-loader',
        ],
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'babel-loader',
          },
        ],
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: jsLoaders(),
      },
      {
        test: /\.(png|jpg|svg|gif|webp|jpeg)$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'file-loader',
          },
          {
            loader: ImageminPlugin.loader,
            options: {
              bail: false,
              cache: false,
              imageminOptions: {
                plugins: [
                  ['pngquant', {quality: [0.6, 0.6]}],
                  ['mozjpeg', {quality: 60, progressive: true}],
                  ['gifsicle', {interlaced: true, optimizationLevel: 3}],
                  ['svgo', {
                    plugins: [
                      {removeViewBox: false}
                    ]
                  }]
                ]
              }
            }
          },
        ],
      },
    ],
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: '[name].css',
    }),
    new HtmlWebpackPlugin({
      template: './index.html',
    }),
  ],
};