let addTweetButton = variables.addTweetButton;
let saveModifiedItem = variables.saveModifiedItem;
let tweets = variables.tweets;
let list = variables.list;
let modifyItemInput = variables.modifyItemInput;
let cancelModification = variables.cancelModification;
let navigationButtons = variables.navigationButtons;

modifyItemInput.maxLength = 139;
let tweetsString = localStorage.getItem('tweets');
tweets = JSON.parse(tweetsString);
if (tweets !== null) {
    let haveLikedTweets;
    tweets.forEach(tweet => {

        if (tweet.liked) {
            haveLikedTweets = true;
        }

        let listLi = document.createElement('li');
        listLi.innerText = tweet.message;
        listLi.id = tweet.id;
        list.append(listLi);
        listLi.addEventListener('click', editTweet)

        let listLiButtonRemove = document.createElement('button');
        listLiButtonRemove.id = tweet.id;
        listLiButtonRemove.addEventListener('click', function () {
            let index = tweets.indexOf(tweet);
            tweets.splice(index, 1)
            localStorage.setItem('tweets', JSON.stringify(tweets));
            location.replace(location.pathname);
        });

        listLiButtonRemove.innerText = 'Remove';
        listLi.append(listLiButtonRemove);
        let listLiButtonLike = document.createElement('button');
        if (tweet.liked) {
            listLiButtonLike.innerText = 'Unlike';
        } else {
            listLiButtonLike.innerText = 'Like';
        }
        listLi.append(listLiButtonLike);

        listLiButtonLike.addEventListener('click', function () {

            if (!tweet.liked) {
                tweet.liked = true;
            } else {
                tweet.liked = false;
            }
            localStorage.setItem('tweets', JSON.stringify(tweets));
            location.replace(location.pathname);
        });
    });

    let goToLikedButton = document.createElement('button');
    if (haveLikedTweets) {
        goToLikedButton.innerText = 'Go to liked';
        navigationButtons.append(goToLikedButton);
    }
    goToLikedButton.addEventListener('click', function () {
        location.hash = '#/liked';
    });

} else {
    tweets = [];
}

addTweetButton.addEventListener('click', function () {
    location.hash = '#/add';
});

window.addEventListener('hashchange', function () {
    let hash = location.hash;

    if (hash === '#/add') {
        showAddOrEditTweetPage('Add Tweet');
        saveModifiedItem.addEventListener('click', addTweet);
    } else if (hash.startsWith('#/edit/:')) {
        showAddOrEditTweetPage('Edit Tweet');
        fillTextArea();
        saveModifiedItem.addEventListener('click', editTweetMessage);
    } else if (hash === '#/liked') {
        let headerLikedTweets = document.getElementsByTagName('h1')[0];
        headerLikedTweets.innerText = 'Liked Tweets';
        list.innerText = '';
        showLikedTweets(tweets);
    }
});

cancelModification.addEventListener('click', function () {
    location.replace(location.pathname);
});

import * as variables from './DOMvariables.js';
import {editTweet, addTweet, editTweetMessage, fillTextArea} from './tweetsUtils.js';
import {showAddOrEditTweetPage, showLikedTweets} from './view.js';