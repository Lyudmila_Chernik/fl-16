let listId;
function editTweet(event) {
    if (event.target.localName === 'li') {
        location.hash = '#/edit/:' + event.target.id;
        listId = event.target.id;
    }
}

function addTweet() {
    let tweetsString = localStorage.getItem('tweets');
    let tweets = JSON.parse(tweetsString);
    let modifyItemInput = document.getElementById('modifyItemInput');
    let inputMessage = modifyItemInput.value;
    let shouldBePushed = true;
    if (tweets.length !== 0) {
        tweets.forEach(element => {
            let keyValue = element.message;
            if (inputMessage === keyValue) {
                shouldBePushed = false;
            }
        });
    }
    if (shouldBePushed) {
        let tweet = { id: tweets.length, message: inputMessage, liked: false };
        tweets.push(tweet);
        localStorage.setItem('tweets', JSON.stringify(tweets));
        console.log(location.pathname);
        location.replace(location.pathname);
    } else {
        alert('Error! You can\'t tweet about that');
    }
}

function editTweetMessage() {
    let modifyItemInput = document.getElementById('modifyItemInput');
    let tweetsStr = localStorage.getItem('tweets');
    let tweetsArr = JSON.parse(tweetsStr);
    tweetsArr.forEach(tweet => {
        if (tweet.id === +listId) {
            tweet.message = modifyItemInput.value;
        }
    });
    localStorage.setItem('tweets', JSON.stringify(tweetsArr));
    location.replace(location.pathname);
}

function fillTextArea() {
    let tweetsStr = localStorage.getItem('tweets');
    let tweetsArr = JSON.parse(tweetsStr);
    tweetsArr.forEach(tweet => {
        if (tweet.id === +listId) {
            let modifyItemInput = document.getElementById('modifyItemInput');
            modifyItemInput.innerText = tweet.message;
        }
    });
}

export { editTweet, addTweet, editTweetMessage, fillTextArea, listId }