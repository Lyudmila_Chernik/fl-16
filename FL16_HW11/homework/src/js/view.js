function showAddOrEditTweetPage(headerText) {
    let modifyItemHeader = document.getElementById('modifyItemHeader');
    modifyItemHeader.innerText = headerText;
    let classListVar = document.getElementById('modifyItem');
    classListVar.classList.toggle('hidden');
    let tweetItems = document.getElementById('tweetItems');
    tweetItems.classList.toggle('hidden');
    tweetItems.classList.toggle('listPage');

}

function showLikedTweets() {
    let list = document.getElementById('list');
    list.innerText = ''
    let tweetsString = localStorage.getItem('tweets');
    let tweets = JSON.parse(tweetsString);
    let likedTweets = tweets.filter(tweet => tweet.liked);
    if (likedTweets.length === 0) {
        location.replace(location.pathname);
    } else {
        likedTweets.forEach(tweet => {
            let listLi = document.createElement('li');
            listLi.innerText = tweet.message;
            listLi.id = tweet.id;
            list.append(listLi);
            listLi.addEventListener('click', editTweet)

            let listLiButtonRemove = document.createElement('button');
            listLiButtonRemove.id = tweet.id;
            listLiButtonRemove.addEventListener('click', function () {
                let index = tweets.indexOf(tweet);
                tweets.splice(index, 1)
                localStorage.setItem('tweets', JSON.stringify(tweets));
            });

            listLiButtonRemove.innerText = 'Remove';
            listLi.append(listLiButtonRemove);
            let listLiButtonLike = document.createElement('button');
            listLi.append(listLiButtonLike);
            if (tweet.liked) {
                listLiButtonLike.innerText = 'Unlike';
            } else {
                listLiButtonLike.innerText = 'Like';
            }

            listLiButtonLike.addEventListener('click', function () {

                if (!tweet.liked) {
                    tweet.liked = true;
                } else {
                    tweet.liked = false;
                }
                localStorage.setItem('tweets', JSON.stringify(tweets));
                location.hash = '#/liked';
                showLikedTweets();

            });

        });
    }
}

import { editTweet } from './tweetsUtils';
export { showAddOrEditTweetPage, showLikedTweets }