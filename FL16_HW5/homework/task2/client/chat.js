const userName = prompt('Enter your name');

const socket = new WebSocket('ws://localhost:8080');

document.forms.publish.onsubmit = function () {
  const outgoingMessage = this.message.value;
  showMessage(outgoingMessage, userName, true);
  const objString = createMessage(outgoingMessage, userName);
  socket.send(objString);
  this.reset();
  return false;
};

socket.onmessage = function (event) {
  const incomingMessage = event.data;
  const messageObj = JSON.parse(incomingMessage);
  showMessage(messageObj.message, messageObj.name, false);
};

function showMessage(message, name, outcoming) {

  const userNameElem = document.createElement('div');
  userNameElem.classList.add('userNameElem');
  if (outcoming) {
    userNameElem.classList.add('outcoming-message');
  }
  userNameElem.appendChild(document.createTextNode(name));
  document.getElementById('chat_window').appendChild(userNameElem);

  const messageElem = document.createElement('div');
  messageElem.classList.add('messageElem');
  if (outcoming) {
    messageElem.classList.add('outcoming-message');
  }
  const messageElementText = document.createTextNode(message);
  messageElem.appendChild(messageElementText);
  document.getElementById('chat_window').appendChild(messageElem);

  const timeElem = document.createElement('div');
  timeElem.classList.add('timeElem');
  if (outcoming) {
    timeElem.classList.add('outcoming-message');
  }
  timeElem.appendChild(document.createTextNode(createTimeStamp()));
  document.getElementById('chat_window').appendChild(timeElem);
}

function createTimeStamp() {
  const timeStamp = new Date();
  const hours = timeStamp.getHours();
  const minutes = timeStamp.getMinutes();
  return hours + ':' + minutes;
}

function createMessage(message, name) {
  const obj = {
    name: name,
    message: message
  }
  return JSON.stringify(obj);
}