showSpinner()
fetch('https://jsonplaceholder.typicode.com/users')
    .then(response => response.json())
    .then(json => listUsers(json))

function listUsers(json) {
    const tbody_users = document.getElementById('tbody_users');
    json.forEach(user => {
        fillTableRow(user, tbody_users);
    })
}

function fillTableRow(user, tbody) {
    const tr = document.createElement('tr');
    tr.contentEditable = 'false';

    let tdId = document.createElement('td');
    tdId.innerText = user.id;
    tr.append(tdId);

    let tdName = document.createElement('td');
    tdName.innerText = user.name;
    tr.append(tdName);

    let tdUserName = document.createElement('td');
    tdUserName.innerText = user.username;
    tr.append(tdUserName);

    let tdEmail = document.createElement('td');
    tdEmail.innerText = user.email;
    tr.append(tdEmail);

    let tdAddress = document.createElement('td');
    tdAddress.innerText = user.address.city;
    tr.append(tdAddress);

    let tdPhone = document.createElement('td');
    tdPhone.innerText = user.phone;
    tr.append(tdPhone);

    let tdWebsite = document.createElement('td');
    tdWebsite.innerText = user.website;
    tr.append(tdWebsite);

    let tdCompany = document.createElement('td');
    tdCompany.innerText = user.company.name;
    tr.append(tdCompany);

    let tdEdit = document.createElement('td');
    const editButton = document.createElement('button');
    editButton.innerText = 'Edit';
    editButton.id = `edit${user.id}`;
    tdEdit.append(editButton);
    tr.append(tdEdit);

    let tdRemove = document.createElement('td');
    const removeButton = document.createElement('button');
    removeButton.innerText = 'Remove';
    removeButton.id = `remove${user.id}`;
    tdRemove.append(removeButton);
    tr.append(tdRemove);
    tbody.append(tr);

    editButton.addEventListener('click', makeEditableContent);
    removeButton.addEventListener('click', removeUser);
}

function removeUser(event) {
    let removeButton = event.target;
    const row = removeButton.parentElement.parentElement;
    let tableData = row.cells;
    const id = tableData[0].innerText;
    showSpinner();
    row.remove();
    fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {
        method: 'DELETE'
    });
}

function makeEditableContent(event) {
    let editButton = event.target;
    editButton.innerText = 'Save';
    editButton.parentElement.parentElement.contentEditable = 'true';
    editButton.contentEditable = 'false';
    editButton.addEventListener('click', saveEditableContent);
}

function saveEditableContent(event) {
    event.target.parentElement.parentElement.contentEditable = 'false';
    event.target.innerText = 'Edit';
    let tableData = event.target.parentElement.parentElement.cells;
    showSpinner();
    fetch(`https://jsonplaceholder.typicode.com/users/${tableData[0].innerText}`, {
        method: 'PUT',
        body: JSON.stringify(createUserFromTableRow(tableData)),
        headers: {
            'Content-type': 'application/json; charset=UTF-8'
        }
    })
        .then((response) => response.json())
        .then((json) => console.log(json));
}

function createUserFromTableRow(rowDataArray) {
    let obj = {
        id: rowDataArray[0].innerText,
        name: rowDataArray[1].innerText,
        username: rowDataArray[2].innerText,
        email: rowDataArray[3].innerText,
        address: rowDataArray[4].innerText,
        phone: rowDataArray[5].innerText,
        website: rowDataArray[6].innerText,
        company: rowDataArray[7].innerText
    }
    return obj;
}

function showSpinner() {
    const spinner = document.getElementById('spinner');
    const table = document.getElementById('table');
    const spinnerShowTime = 3000;
    table.style.display = 'none';
    setTimeout(hideSpinner, spinnerShowTime);
    spinner.style.display = '';
}

function hideSpinner() {
    const table = document.getElementById('table');
    document.getElementById('spinner').style.display = 'none';
    table.style.display = '';
}