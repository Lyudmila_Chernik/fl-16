function isEquals(a, b) {
    return a === b;
}

function isBigger(a, b) {
    return a > b;
}

function storeNames() {
    let resArray = [];
    for (let i = 0; i < arguments.length; i++) {
        resArray.push(arguments[i]);
    }
    return resArray;
}

function getDifference(a, b) {
    if (a > b) {
        return a - b;
    } else {
        return b - a;
    }
}

function negativeCount(array) {
    let count = 0;
    for (let i = 0; i < array.length; i++) {
        if (array[i] < 0) {
            count++;
        }
    }
    return count;
}

function letterCount(a, b) {
    let count = 0;
    for (let i = 0; i < a.length; i++) {
        if (a[i] === b) {
            count++;
        }
    }
    return count;
}

function countPoints(array) {
    let totalCount = 0;
    let win = 3;
    let drawn = 1;
    for (let i = 0; i < array.length; i++) {
        let element = array[i];
        let scoreArray = element.split(':');
        let firstTeamScore = +scoreArray[0];
        let secondTeamScore = +scoreArray[1];

        if (firstTeamScore > secondTeamScore) {
            totalCount = totalCount + win;
        } else if (firstTeamScore === secondTeamScore) {
            totalCount = totalCount + drawn;
        }
    }
    return totalCount;
}