function visitLink(path) {

	if (localStorage.getItem(path)) {
		let clickCount = localStorage.getItem(path);
		localStorage.setItem(path, ++clickCount);
	} else {
		localStorage.setItem(path, 1);
	}
}

function viewResults() {

	let openString = '<ol>';
	let closeString = '</ol>';
	let resultString = '';
	let keys = Object.keys(localStorage);
	for (let i = 0; i < keys.length; i++) {
		let value = localStorage[keys[i]];
		resultString = resultString + '<li>You visited  ' + keys[i] + ' ' + value + ' times.</li>';
	}

	let content = openString + resultString + closeString
	document.querySelector('#content').insertAdjacentHTML('beforeend', content);
	localStorage.clear();
}