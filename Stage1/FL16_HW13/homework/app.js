const appRoot = document.getElementById('app-root');

const headerH1 = document.createElement('h1');
headerH1.className = 'headerText';
headerH1.innerText = 'Countries Search';
appRoot.append(headerH1);

const textAndRadio = document.createElement('div');
textAndRadio.className = 'textAndRadio';
let textAndRadioForm = document.createElement('form');

const textAndRadiosWrap = document.createElement('div');
textAndRadiosWrap.className = 'textAndRadiosWrap';
textAndRadioForm.append(textAndRadiosWrap);

textAndRadio.append(textAndRadioForm);

headerH1.after(textAndRadio);

const textBefRadio = document.createElement('div');
textBefRadio.className = 'textBefRadio';
textBefRadio.innerText = 'Please choose type of search';
textAndRadiosWrap.append(textBefRadio);

const radioWrap = document.createElement('div');
radioWrap.className = 'radioWrap';
textBefRadio.after(radioWrap);

const radioRegions = document.createElement('input');
radioRegions.type = 'radio';
radioRegions.name = 'radioSearchTypeSelection';
radioRegions.id = 'radioOne';
radioWrap.append(radioRegions);

const regionsLabel = document.createElement('label');
regionsLabel.for = radioRegions.id;
regionsLabel.innerText = 'By Region ';
radioRegions.after(regionsLabel);

const radioLanguage = document.createElement('input');
radioLanguage.type = 'radio';
radioLanguage.name = 'radioSearchTypeSelection';
radioLanguage.id = 'radioTwo';
radioWrap.append(radioLanguage);

const languageLabel = document.createElement('label');
languageLabel.for = radioLanguage.id;
languageLabel.innerText = 'By Language ';
radioLanguage.after(languageLabel);

const textAndSelectWrap = document.createElement('div');
textAndSelectWrap.className = 'textAndSelectWrap';
textAndRadiosWrap.append(textAndSelectWrap);

const textBefSelect = document.createElement('div');
textBefSelect.className = 'textBefSelect';
textBefSelect.innerText = 'Please choose search query: ';
textAndSelectWrap.append(textBefSelect);

const querySelect = document.createElement('select');
const defaultSelectOption = document.createElement('option');
defaultSelectOption.hidden = true;
defaultSelectOption.selected = true;
defaultSelectOption.innerText = 'Select value';
querySelect.append(defaultSelectOption);
querySelect.className = 'querySelect';
querySelect.disabled = true;

querySelect.id = 'querySelectId';
textBefSelect.append(querySelect);

const tableWrap = document.createElement('div');
tableWrap.className = 'tableWrap';
textAndRadio.after(tableWrap);

radioRegions.addEventListener('change', function () {
  querySelect.length = 0;
  querySelect.name = 'region';
  querySelect.disabled = false;
  tableWrap.textContent = 'No items, please choose search query';
  const regionsList = externalService.getRegionsList();
  regionsList.forEach(region => {
    let optionElement = document.createElement('option');
    optionElement.innerText = region;
    querySelect.append(optionElement);
  });
});

radioLanguage.addEventListener('change', function () {
  querySelect.length = 0;
  querySelect.name = 'language';
  querySelect.disabled = false;
  const langsList = externalService.getLanguagesList();
  langsList.forEach(language => {
    let optionElement = document.createElement('option');
    optionElement.innerText = language;
    querySelect.append(optionElement);
  });
});

const ascendingSortName = (a, b) => {
  const less = -1;
  if (a.name < b.name) {
    return less;
  } else if (a.name > b.name) {
    return 1;
  }
  return 0;
};

const descendingSortName = (a, b) => {
  const less = -1;
  if (a.name < b.name) {
    return 1;
  } else if (a.name > b.name) {
    return less;
  }
  return 0;
};

const ascendingSortArea = (a, b) => {
  const less = -1;
  if (a.area < b.area) {
    return less;
  } else if (a.area > b.area) {
    return 1;
  }
  return 0;
};

const descendingSortArea = (a, b) => {
  const less = -1;
  if (a.area < b.area) {
    return 1;
  } else if (a.area > b.area) {
    return less;
  }
  return 0;
};

let sortFunction;
let countryArrow;
querySelect.addEventListener('change', function (e) {
  sortFunction = ascendingSortName;
  let returnedList;
  tableWrap.textContent = '';
  let selectedOption = e.target.value;
  if (querySelect.name === 'region') {
    returnedList = externalService.getCountryListByRegion(selectedOption);
  } else {
    returnedList = externalService.getCountryListByLanguage(selectedOption);
  }
  returnedList.sort(sortFunction);
  let countryButton = document.createElement('button');
  countryButton.innerText = '↑';
  let areaButton = document.createElement('button');
  areaButton.innerText = '↕';

  tableWrap.append(createTable(returnedList, countryButton, areaButton));

  countryButton = document.getElementsByClassName('arrowCountry')[0];
  countryButton.addEventListener('click', function () {
    areaButton.innerText = '↕';
    if (sortFunction === ascendingSortName) {
      sortFunction = descendingSortName;
      countryButton.innerText = '↓';
    } else {
      countryButton.innerText = '↑';
      sortFunction = ascendingSortName;
    }

    returnedList.sort(sortFunction);
    tableWrap.innerText = '';
    tableWrap.append(createTable(returnedList, countryButton, areaButton));
  });

  areaButton = document.getElementsByClassName('arrowArea')[0];
  areaButton.addEventListener('click', function () {
    countryButton.innerText = '↕';
    if (sortFunction === ascendingSortArea) {
      sortFunction = descendingSortArea;
      areaButton.innerText = '↓';
    } else {
      sortFunction = ascendingSortArea;
      areaButton.innerText = '↑';
    }
    returnedList.sort(sortFunction);
    tableWrap.innerText = '';
    tableWrap.append(createTable(returnedList, countryButton, areaButton));
  });

});

function createTable(data, countryButton, areaButton) {
  let table = document.createElement('table');
  table.className = 'table';
  table = createHeadForTable(table, countryButton, areaButton);
  const tableBody = document.createElement('tbody');
  table.append(tableBody);

  data.forEach((country) => {
    let row = document.createElement('tr');

    let countryName = document.createElement('td');
    countryName.innerText = country.name;
    row.append(countryName);

    let capital = document.createElement('td');
    capital.innerText = country.capital;
    row.append(capital);

    let worldRegion = document.createElement('td');
    worldRegion.innerText = country.region;
    row.append(worldRegion);

    let languages = document.createElement('td');
    let languagesList = country.languages;
    let resultLanguageString = '';
    Object.values(languagesList).forEach((item) => {
      resultLanguageString = resultLanguageString + item + ', '
    });
    languages.innerText = resultLanguageString;
    row.append(languages);

    let area = document.createElement('td');
    area.innerText = country.area;
    row.append(area);

    let flag = document.createElement('td');
    let flagImage = document.createElement('img');
    flagImage.src = country.flagURL;
    flag.append(flagImage);
    row.append(flag);

    tableBody.append(row);
  });
  return table;
};

const table = document.createElement('table');
table.className = 'table';
tableWrap.append(table);

function createHeadForTable(table, countryButton, areaButton) {

  const tableHead = document.createElement('thead');
  tableHead.className = 'tableHead';
  table.append(tableHead);

  const headRow = document.createElement('tr');
  tableHead.append(headRow);

  const th1 = document.createElement('th');
  th1.classList.add('th1', 'countryArrow')
  th1.innerText = 'Country Name';
  headRow.append(th1);

  const arrowCountry = countryButton;
  arrowCountry.className = 'arrowCountry';
  th1.append(arrowCountry);

  const th2 = document.createElement('th');
  th2.innerText = 'Capital';
  th2.className = 'th';
  headRow.append(th2);

  const th3 = document.createElement('th');
  th3.innerText = 'World Region';
  th3.className = 'th';
  headRow.append(th3);

  const th4 = document.createElement('th');
  th4.innerText = 'Languages';
  th4.className = 'th';
  headRow.append(th4);

  const th5 = document.createElement('th');
  th5.innerText = 'Area';
  th5.classList.add('th5', 'areaArrow')
  headRow.append(th5);

  const arrowArea = areaButton;
  arrowArea.className = 'arrowArea';
  th5.append(arrowArea);

  const th6 = document.createElement('th');
  th6.innerText = 'Flag';
  th6.className = 'th';
  headRow.append(th6);

  return table;
}