// Your code goes here
window.onload = function () {
    prompt('Enter event name', 'meeting');
    let button = document.getElementById('confirmButton');

    button.onclick = function () {
        let inputIsValid = true;
        let nameInputField = document.getElementById('enterName');
        if (nameInputField.value !== '') {
            nameInputField.style.backgroundColor = '#bfa';
        } else {
            nameInputField.style.backgroundColor = '#fba';
            inputIsValid = false;
        }

        let timeInputField = document.getElementById('enterTime');

        if (timeInputField.value === '') {
            inputIsValid = false;
        } else {
            inputIsValid = validateHhMm(timeInputField);
        }

        let placeInputField = document.getElementById('enterPlace');
        if (placeInputField.value !== '') {
            placeInputField.style.backgroundColor = '#bfa';
        } else {
            placeInputField.style.backgroundColor = '#fba';
            inputIsValid = false;
        }

        if (inputIsValid) {
            console.log(nameInputField.value + ' has a meeting today at '
                + timeInputField.value + ' somewhere in ' + placeInputField.value);
        } else {
            alert('Input all data');
        }
    }

    button = document.getElementById('converterButton');
    button.onclick = function () {
        const radix = 2;
        let exchangeEuro = 33.52;
        let exchangeDollar = 27.76;
        let amountOfEuro = prompt('Input amount of euro');
        let amountOfDollar = prompt('Input amount of dollar');
        let validEuro = isCurrecyInputValid(amountOfEuro);
        let validDollar = isCurrecyInputValid(amountOfDollar);

        if (validEuro && validDollar) {
            let euroIntoHryvnia = amountOfEuro * exchangeEuro;
            let dollarIntoHryvnia = amountOfDollar * exchangeDollar;
            alert(amountOfEuro + ' euros are equal ' + euroIntoHryvnia.toFixed(radix) + 'hrns'
                + ', ' + amountOfDollar + ' dollars are equal ' + dollarIntoHryvnia.toFixed(radix) + 'hrns.');
        } else {
            alert('Not valid input')
        }
    }
};

function isCurrecyInputValid(inputValue) {
    return inputValue > 0 && Number.isInteger(+inputValue);
}

function validateHhMm(inputField) {
    let regex = /^(2[0-3]|[01]?[0-9]):([0-5]?[0-9])$/;
    let isValid = regex.test(inputField.value);
    if (isValid) {
        inputField.style.backgroundColor = '#bfa';
    } else {
        inputField.style.backgroundColor = '#fba';
        alert('Enter time in format hh:mm');
    }
    return isValid;
}
