const root = document.getElementById('root');
let addTweetButton = document.getElementsByClassName('addTweet')[0];
let tweetItems = document.getElementById('tweetItems');
let modifyItemHeader = document.getElementById('modifyItemHeader');
let saveModifiedItem = document.getElementById('saveModifiedItem');
let tweets = [];
let list = document.getElementById('list');
let tweetsString = localStorage.getItem('tweets');
let modifyItemInput = document.getElementById('modifyItemInput');
let cancelModification = document.getElementById('cancelModification');
let navigationButtons = document.getElementById('navigationButtons');

modifyItemInput.maxLength = 139;

tweets = JSON.parse(tweetsString);
if (tweets !== null) {
    let haveLikedTweets;
    tweets.forEach(tweet => {

        if (tweet.liked) {
            haveLikedTweets = true;
        }

        let listLi = document.createElement('li');
        listLi.innerText = tweet.message;
        listLi.id = tweet.id;
        list.append(listLi);
        listLi.addEventListener('click', editTweet)

        let listLiButtonRemove = document.createElement('button');
        listLiButtonRemove.id = tweet.id;
        listLiButtonRemove.addEventListener('click', function () {
            let index = tweets.indexOf(tweet);
            tweets.splice(index, 1)
            localStorage.setItem('tweets', JSON.stringify(tweets));
            location.replace(location.pathname);
        });

        listLiButtonRemove.innerText = 'Remove';
        listLi.append(listLiButtonRemove);
        let listLiButtonLike = document.createElement('button');
        if (tweet.liked) {
            listLiButtonLike.innerText = 'Unlike';
        } else {
            listLiButtonLike.innerText = 'Like';
        }
        listLi.append(listLiButtonLike);

        listLiButtonLike.addEventListener('click', function () {

            if (!tweet.liked) {
                tweet.liked = true;
            } else {
                tweet.liked = false;
            }
            localStorage.setItem('tweets', JSON.stringify(tweets));
            location.replace(location.pathname);
        });
    });

    let goToLikedButton = document.createElement('button');
    if (haveLikedTweets) {
        goToLikedButton.innerText = 'Go to liked';
        navigationButtons.append(goToLikedButton);
    }
    goToLikedButton.addEventListener('click', function () {
        location.hash = '#/liked';
    });

} else {
    tweets = [];
}

addTweetButton.addEventListener('click', function () {
    location.hash = '#/add';
});

let listId;
window.addEventListener('hashchange', function () {
    let hash = location.hash;

    if (hash === '#/add') {
        showAddOrEditTweetPage('Add Tweet');
        saveModifiedItem.addEventListener('click', addTweet);
    } else if (hash.startsWith('#/edit/:')) {
        showAddOrEditTweetPage('Edit Tweet');
        fillTextArea();
        saveModifiedItem.addEventListener('click', editTweetMessage);
    } else if (hash === '#/liked') {
        let headerLikedTweets = document.getElementsByTagName('h1')[0];
        headerLikedTweets.innerText = 'Liked Tweets';
        list.innerText = '';
        showLikedTweets();
    }
});

function showLikedTweets() {
    tweets.forEach(tweet => {

        if (tweet.liked) {

            let listLi = document.createElement('li');
            listLi.innerText = tweet.message;
            listLi.id = tweet.id;
            list.append(listLi);
            listLi.addEventListener('click', editTweet)

            let listLiButtonRemove = document.createElement('button');
            listLiButtonRemove.id = tweet.id;
            listLiButtonRemove.addEventListener('click', function () {
                let index = tweets.indexOf(tweet);
                tweets.splice(index, 1)
                localStorage.setItem('tweets', JSON.stringify(tweets));
            });

            listLiButtonRemove.innerText = 'Remove';
            listLi.append(listLiButtonRemove);
            let listLiButtonLike = document.createElement('button');
            if (tweet.liked) {
                listLiButtonLike.innerText = 'Unlike';
            } else {
                listLiButtonLike.innerText = 'Like';
            }

            listLiButtonLike.addEventListener('click', function () {

                if (!tweet.liked) {
                    tweet.liked = true;
                } else {
                    tweet.liked = false;
                }
                localStorage.setItem('tweets', JSON.stringify(tweets));

            });
        }
    });
}


function editTweet(event) {
    if (event.target.localName === 'li') {
        location.hash = '#/edit/:' + event.target.id;
        listId = event.target.id;
    }
}

function showAddOrEditTweetPage(headerText) {
    modifyItemHeader.innerText = headerText;
    let classListVar = document.getElementById('modifyItem');
    classListVar.classList.toggle('hidden');
    tweetItems.classList.toggle('hidden');
}

function addTweet() {
    let modifyItemInput = document.getElementById('modifyItemInput');
    let inputMessage = modifyItemInput.value;
    let shouldBePushed = true;
    if (tweets.length !== 0) {
        tweets.forEach(element => {
            let keyValue = element.message;
            if (inputMessage === keyValue) {
                shouldBePushed = false;
            }
        });
    }
    if (shouldBePushed) {
        let tweet = { id: tweets.length, message: inputMessage, liked: false };
        tweets.push(tweet);
        localStorage.setItem('tweets', JSON.stringify(tweets));
        console.log(location.pathname);
        location.replace(location.pathname);
    } else {
        alert('Error! You can\'t tweet about that');
    }
}

function editTweetMessage() {
    let modifyItemInput = document.getElementById('modifyItemInput');
    let tweetsStr = localStorage.getItem('tweets');
    let tweetsArr = JSON.parse(tweetsStr);
    tweetsArr.forEach(tweet => {
        if (tweet.id === +listId) {
            tweet.message = modifyItemInput.value;
        }
    });
    localStorage.setItem('tweets', JSON.stringify(tweetsArr));
    location.replace(location.pathname);
}

function fillTextArea() {
    let tweetsStr = localStorage.getItem('tweets');
    let tweetsArr = JSON.parse(tweetsStr);
    tweetsArr.forEach(tweet => {
        if (tweet.id === +listId) {
            modifyItemInput.innerText = tweet.message;
        }
    });
}

cancelModification.addEventListener('click', function () {
    location.replace(location.pathname);
});