function reverseNumber(num) {
    let stringNum = num + '';
    let resultString = '';
    let lastChar = 0;
    if (stringNum[0] === '-') {
        lastChar = 1;
        resultString = stringNum[0];
    }
    for (let i = stringNum.length - 1; i >= lastChar; i--) {
        let char = stringNum[i];
        resultString = resultString + char;
    }
    return +resultString;
}

function logInConsole(el) {
    console.log(el);
}

function forEach(arr, func) {
    let resultArray = []
    for (let i = 0; i < arr.length; i++) {
        resultArray.push(func(arr[i]));
    }
    return resultArray;
}

function map(arr, func) {
    return forEach(arr, func);
}

function filter(arr, func) {
    let resultArray = [];
    let booleanArray = forEach(arr, func);
    for (let i = 0; i < arr.length; i++) {
        if (booleanArray[i] === true) {
            resultArray.push(arr[i]);
        }
    }
    return resultArray;
}

function getAdultAppleLovers(data) {
    let namesArray = [];
    let neededAge = 18;
    let resultArray = filter(data, function (el) {
        return el.age > neededAge;
    });
    let adultsWithApples = filter(resultArray, function (el) {
        return el.favoriteFruit === 'apple'
    });

    for (let i = 0; i < adultsWithApples.length; i++) {
        let somebodyName = adultsWithApples[i].name;
        namesArray.push(somebodyName);
    }
    return namesArray;
}

function getKeys(obj) {
    let keyArray = [];
    for (let key in obj) {
        if (true) {
            keyArray.push(key);
        }
    }
    return keyArray;
}

function getValues(obj) {
    let valueArray = [];

    for (let key in obj) {
        if (true) {
            valueArray.push(obj[key]);
        }
    }
    return valueArray;
}

function showFormattedDate(dateObj) {
    let dateStr = '' + dateObj;
    let monthMin = 3;
    let monthMax = 7;
    let dayOfMonthMax = 10;
    let yearMax = 15
    let month = '';
    let dayOfMonth = '';
    let year = '';

    for (let i = 0; i < dateStr.length; i++) {

        if (i > monthMin && i < monthMax) {
            month = month + dateStr[i];
        } else if (i > monthMax && i < dayOfMonthMax) {
            dayOfMonth = dayOfMonth + dateStr[i];
        } else if (i > dayOfMonthMax && i < yearMax) {
            year = year + dateStr[i];
        }

    }
    return dayOfMonth + ' of ' + month + ', ' + year;
}
