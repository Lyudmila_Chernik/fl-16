let two = 2;
let ballOffset = 17;
let threeSeconds = 3000;

let table = document.getElementsByTagName('table')[0];
let tr = document.getElementsByTagName('tr');
let td = document.getElementsByTagName('td');

table.addEventListener('click', function (event) {

    if (event.target === event.target.closest('tr').cells.item(0)) {
        event.target.closest('tr').style = 'background-color: blue';
        event.stopPropagation();
    } else {
        event.target.style = 'background-color: yellow';
        event.stopPropagation();
    }

    if (event.target.innerText === 'Special Cell') {
        let tdArray = document.getElementsByTagName('td')
        for (let i = 0; i < tdArray.length; i++) {
            if (tdArray[i].closest('tr').style.backgroundColor !== 'blue') {
                tdArray[i].style = 'background-color: yellow';
            }
        }
        event.stopPropagation();
    }
});

let input = document.getElementById('phone');
let inputSubmit = document.getElementsByClassName('inputSubmit')[0];
let validationMessage = document.getElementsByClassName('validationMessage')[0];
let inputPhone = document.getElementsByClassName('inputPhone')[0];
inputSubmit.disabled = true;

function validationPhone() {
    const inputRegExp = /\+380[0-9]{two}[0-9]{7}/;
    return inputRegExp.test(input.value);
}

input.addEventListener('input', function () {

    if (validationPhone()) {
        validationMessage.style = 'visibility: hidden';
        inputPhone.style = 'border: 1px solid green; background-color: lightgreen';
        inputSubmit.disabled = false;
    } else {
        validationMessage.innerText = 'Type number does not follow format +380*********';
        validationMessage.style = 'visibility: visible; border: 1px solid red; background-color: lightpink';
        inputPhone.style = 'border: 1px solid red; background-color: lightpink';
        inputSubmit.disabled = true;
    }
});

inputSubmit.addEventListener('click', function () {
    validationMessage.innerText = 'Data was succesfully sent';
    validationMessage.style = 'visibility: visible; border: 1px solid green; background-color: lightgreen';
});

let ball = document.getElementById('ball');

let field = document.getElementById('task3');
ball.style.left = Math.round(field.clientWidth / two - ball.offsetWidth / two) + 'px';
ball.style.top = Math.round(field.clientHeight / two - ball.offsetHeight / two) + ballOffset + 'px';
field.addEventListener('click', function (event) {

    let fieldCoords = this.getBoundingClientRect();

    let ballCoords = {
        top: event.clientY - fieldCoords.top - field.clientTop - ball.clientHeight / two,
        left: event.clientX - fieldCoords.left - field.clientLeft - ball.clientWidth / two
    };

    if (ballCoords.top < 0) {
        ballCoords.top = 0;
    }

    if (ballCoords.left < 0) {
        ballCoords.left = 0;
    }

    if (ballCoords.left + ball.clientWidth > field.clientWidth) {
        ballCoords.left = field.clientWidth - ball.clientWidth;
    }

    if (ballCoords.top + ball.clientHeight > field.clientHeight) {
        ballCoords.top = field.clientHeight - ball.clientHeight;
    }

    ball.style.left = ballCoords.left + 'px';
    ball.style.top = ballCoords.top + 'px';
});

let scoresA = document.getElementById('scoresA');
let scoresB = document.getElementById('scoresB');

let countA = 0;
let countB = 0;
let countAAppend = document.getElementById('countA');
let countBAppend = document.getElementById('countB');
countAAppend.innerText = 'Team A: ' + countA;
countBAppend.innerText = 'Team B: ' + countB;
let notification = document.getElementById('notification');

scoresA.addEventListener('click', function () {
    countA = countA + 1;
    countAAppend.innerText = 'Team A: ' + countA;
    let eventScore = new CustomEvent('score', { detail: 'Team A score' });
    notification.dispatchEvent(eventScore);
});

scoresB.addEventListener('click', function () {
    countB = countB + 1;
    countBAppend.innerText = 'Team B: ' + countB;
    let eventScore = new CustomEvent('score', { detail: 'Team B score' });
    notification.dispatchEvent(eventScore);
});

notification.addEventListener('score', function (e) {
    let notification = document.getElementById('notification');
    notification.innerText = e.detail;
    window.setTimeout(function () {
        notification.innerText = '';
    }, threeSeconds)
});