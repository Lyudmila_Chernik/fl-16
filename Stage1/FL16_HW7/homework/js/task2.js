// Your code goes here
const maxRangeIncrement = 4;
const attemptsAmount = 3;
const prizeMultiplier = 2;
const prizeDevider = 2;

function getRandomInt(min, max) {
    let minRound = Math.ceil(min);
    let maxRound = Math.floor(max);
    return Math.floor(Math.random() * (maxRound - minRound)) + minRound;
}


const result = confirm('Do you want ot play a game?');
if (result) {
    let minRange = 0;
    let maxRange = 9;
    let randomInt = getRandomInt(minRange, maxRange);
    let atempt = 3;
    let prize = 100;
    let totalPrize = 0;

    while (atempt > 0) {

        let userInt = +prompt('Choose a number from 0 to ' + (maxRange - 1) + '\n'
            + 'Atempts left: ' + atempt + '\n'
            + 'Total prize: ' + totalPrize + '\n'
            + 'Possible prize on current atempt: ' + prize);

        if (randomInt === userInt) {
            totalPrize = totalPrize + prize;
            let continueGame = confirm('‘Congratulation, you won! Your prize is: '
                + totalPrize + '$. Do you want to continue?');

            if (continueGame) {
                maxRange = maxRange + maxRangeIncrement;
                randomInt = getRandomInt(minRange, maxRange);
                atempt = attemptsAmount;
                prize = prize * prizeMultiplier;
            } else {
                break;
            }
        } else {
            atempt--;
            prize = prize / prizeDevider;
        }

    }

    alert('Thank you for your participation. Your prize is: ' + totalPrize + '$');

} else {
    alert('You did not become a billionaire, but can.');
}

