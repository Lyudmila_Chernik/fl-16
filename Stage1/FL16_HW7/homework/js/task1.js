// Your code goes here
const minAmount = 1000;
const maxPercrntage = 100;
const oneHundredPercents = 100;
const radix = 2;

let initialAmount = +prompt('Enter initial amount', '0');
let years = +prompt('Number of year', '0');
let percentages = +prompt('Percentage of year', '0');

let invalidInput = false;

if (isNaN(initialAmount) || isNaN(years) || isNaN(percentages)) {
    invalidInput = true;
} else if (initialAmount < minAmount || years < 1 || percentages > maxPercrntage) {
    invalidInput = true;
} else if (!Number.isInteger(years)) {
    invalidInput = true;
}

if (invalidInput) {
    alert('Invalid input data');
} else {
    let profitPerYear = 0;
    let totalProfit = 0;
    let amount = initialAmount;

    for (let i = 1; i <= years; i++) {
        profitPerYear = amount / oneHundredPercents * percentages;
        totalProfit = totalProfit + profitPerYear;
        amount = amount + profitPerYear;
    }

    alert('Initial amount: ' + initialAmount + '\n'
        + 'Number of year: ' + years + '\n'
        + 'Percentage of year: ' + percentages + '\n\n'
        + 'Total profit: ' + totalProfit.toFixed(radix) + '\n'
        + 'Total amount: ' + amount.toFixed(radix));
}

