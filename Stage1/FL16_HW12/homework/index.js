function getAge(birthDate) {
    const millis = 1000;
    const seconds = 3600;
    const hours = 24;
    const days = 365;

    return Math.trunc((new Date() - birthDate) / (millis * seconds * hours * days));
}

function getWeekDay(incDate) {
    return new Intl.DateTimeFormat('en-US', { weekday: 'long' }).format(incDate);
}

function getAmountDaysToNewYear() {
    const millis = 1000;
    const seconds = 3600;
    const hours = 24;
    const december = 11;
    const newYearDay = 31;
    let currentYear = new Date().getFullYear();
    let newYear = new Date(currentYear, december, newYearDay);
    let now = new Date(currentYear, 0, 1);
    let result = (newYear - now) / (millis * seconds * hours);
    return Math.ceil(result);
}

function getProgrammersDay(yearInt) {
    const programmersDay = 255;
    let incYear = new Date(yearInt, 0, 1);
    incYear.setDate(programmersDay);
    let month = new Intl.DateTimeFormat('en-US', { month: 'short' }).format(incYear);
    let weekDay = getWeekDay(incYear);
    let dayOfMonth = incYear.getDate();
    let year = incYear.getFullYear();

    return dayOfMonth + ' ' + month + ', ' + year + ' (' + weekDay + ').';
}

function howFarIs(specifiedWeekday) {
    specifiedWeekday = specifiedWeekday[0].toUpperCase() + specifiedWeekday.slice(1);
    let currentWeekDay = new Date().getDay();
    let incWeekDay = incWDay(specifiedWeekday);
    let number = Math.abs(currentWeekDay - incWeekDay);

    if (incWeekDay === currentWeekDay) {
        return 'Hey, today is ' + specifiedWeekday + ' =)';
    } else {
        return 'It`s ' + number + ' day(s) left till ' + specifiedWeekday;
    }

    function incWDay(dayName) {
        let namesArr = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
        for (let i = 0; i < namesArr.length; i++) {
            if (dayName === namesArr[i]) {
                return i;
            }
        }
    }
}

function isValidIdentifier(inputValidation) {
    let testRegExp = /^[a-zA-Z_$][a-zA-Z_$0-9]*$/;

    return testRegExp.test(inputValidation);
}

function capitalize(capString) {
    let capRegExp = /(^\w{1})|(\s+\w{1})/g;
    return capString.replace(capRegExp, function (substring) {
        return substring.toUpperCase();
    })
}

function isValidAudioFile(incAudio) {
    let incAudioRegEx = new RegExp('(?=.*[A-Za-z])^[^!@#$%^&*()_][A-Za-z]+[\\.aac|\\.mp3|\\.flac|\\.alac]*$', 'g');

    return incAudioRegEx.test(incAudio);
}

function getHexadecimalColors(incColorString) {
    let incColorRegEx = new RegExp('\\#([a-f0-9]{3}|[a-f0-9]{6})\\b', 'gi');
    let colorsArr = incColorString.match(incColorRegEx);
    if (colorsArr === null) {
        return [];
    }
    return colorsArr;
}

function isValidPassword(incPass) {
    let password = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8}$/;
    return password.test(incPass);
}

function addThousandsSeparators(numbers) {
    let regex = /\B(?=(\d{3})+(?!\d))/g;
    let stringNumbers = numbers.toString();
    let newString = stringNumbers.replace(regex, ',');
    return newString.toString();
}

function getAllUrlsFromText(urlText) {
    const one = 'https?:\\/\\/(www\\.)?[-a-zA-Z0-9@:%._\\+~#=]{1,256}\\.';
    const two = '[a-zA-Z0-9()]{1,6}\\b([-a-zA-Z0-9()@:%_\\+.~#?&//=]*)';
    let regexUrl = new RegExp(one + two, 'g');
    try {
        let matchUrl = urlText.match(regexUrl);

        if (matchUrl === null) {
            return [];
        }
        return matchUrl;
    } catch (error) {
        return error;
    }
}
