class State {
  constructor() {
    this.validForApprove = false;
    this.validForPublish = false;
  }

  validateForApprove(employee) {
    return employee;
  }

  validateForPublish(employee) {
    return employee;
  }

  getInvalidApproveMessage(employee) {
    return employee;
  }

  getInvalidPublishMessage() {
    return 'Invalid message';
  }
}

class ReadyForPushNotificationsMagazineState extends State {

  validateForApprove(employee) {
    this.validForApprove = employee.magazine.isReadyForApprove(employee.categoryName)
  }

  getInvalidApproveMessage(employee) {
    if (!this.validForApprove) {
      return `Hello ${employee.name}. You can't approve. We don't have enough of publications.`;
    }
    return super.getInvalidApproveMessage();
  }

  getInvalidPublishMessage(employee) {
    if (!this.validForPublish) {
      return `Hello ${employee.name}. You can't publish. We are creating publications now.`
    }
    return super.getInvalidPublishMessage();
  }
}

class ReadyForApproveMagazineState extends State {

  constructor() {
    super();
    this.validForApprove = true;
  }

  getInvalidApproveMessage(employee) {
    if (!this.validForApprove) {
      return `Hello ${employee.name} You've approved the changes in the console, and state must be changed.`;
    }
    return '';
  }

  validateForPublish(employee) {
    this.validForPublish = employee.magazine.isReadyForPublish(employee.categoryName);
  }

  getInvalidPublishMessage(employee) {
    if (!this.validForPublish) {
      return `Hello ${employee.name} You can't publish. We don't have a manager's approval.`
    }
    return '';
  }
}

class ReadyForPublishMagazineState {

}

class PublishInProgressMagazineState {

}

const states = {
  readyForPushNotifications: new ReadyForPushNotificationsMagazineState(),
  readyForApprove: new ReadyForApproveMagazineState(),
  readyForPublish: new ReadyForPublishMagazineState(),
  publishInProgress: new PublishInProgressMagazineState()
}

class Article {
  constructor(categoryName, content, author) {
    this.categoryName = categoryName;
    this.content = content;
    this.author = author;
  }
}

class Magazine {
  constructor() {
    this.currentState = states.readyForPushNotifications;
    this.articlesLimitForApproveState = 5;
    this.states = states;
    this.articlesBag = new Map();
  }

  setState(state) {
    this.currentState = state;
  }

  isReadyForApprove(categoryName) {
    const categoryBag = this.articlesBag.get(categoryName);

    if (!categoryBag) {
      return false;
    }

    return categoryBag.length > this.articlesLimitForApproveState;
  }

  addArticle(content, author) {
    const authorData = { ...author };
    const article = new Article(authorData.categoryName, content, authorData);

    let categoryCollection = this.articlesBag.get(authorData.categoryBag);

    if (!categoryCollection) {
      categoryCollection = [];
    }
    categoryCollection.push(article);

    this.articlesBag.set(authorData.categoryName, categoryCollection);

    if (this.isReadyForApprove(authorData.categoryName)) {
      this.setState(states.readyForApprove);
    }
  }
}

class MagazineEmployee {
  constructor(name, categoryName, magazine) {
    this.name = name;
    this.categoryName = categoryName;
    this.magazine = magazine;
  }

  addArticle(content) {
    this.magazine.addArticle(content, this);
  }

  approve() {
    if (this.categoryName === 'manager') {
      const isValidForApprove = states.readyForApprove.validateForApprove(this);
      if (isValidForApprove) {
        this.magazine.setState(states.readyForApprove);
      } else {
        console.error(states.readyForApprove.getInvalidApproveMessage(this));
      }
    }

    console.error('You do not have permissions to do it');
  }
}
