function getMaxEvenElement(arr) {

    let evenArray = arr.filter((element) => {
        return element % 2 === 0;
    })
    return evenArray.reduce((accumulator, currentValue) => {
        return Math.max(accumulator, currentValue);
    })

}

let a = 5;
let b = 3;

[a, b] = [b, a];

console.log(a);
console.log(b);

const getValue = (value) => {
    return value ?? '-';
};

function getObjFromArray(arr) {
    return Object.fromEntries(arr);
}

function addUniqueId(obj) {
    let id = Symbol();
    return { ...obj, id }
}

const oldObj = {
    name: 'willow',
    details: { id: 1, age: 47, university: 'LNU' }
}
const getRegroupedObject = oldObj => {
    const {
        name: firstname,
        details: { id, age, university }
    } = oldObj;
    const user = { age, firstname, id };
    return { university, user };
}

function getArrayWithUniqueElements(arr) {
    let arrSet = new Set(arr);
    return [...arrSet];
}

function hideNumber(number) {
    number = '*****' + number.substring(6, number.split('').length);
    return number;
}

const add = (a, b = 1) => a && b ? a + b : new Error('b is required');


function* generateIterableSequence() {
    yield 'I';
    yield 'love';
    yield 'EPAM';
}
let generatorObject = generateIterableSequence();
for (let value of generatorObject) {
    console.log(value);
}